<?php

namespace Infotechnohelp\Symfony\FileEncryptorBundle\Command;

use Infotechnohelp\Symfony\FileSystemBundle\Service\FileFinder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DecryptDirFiles extends FileEncryptorCommand
{
    protected static $defaultName = 'file-encryptor:decrypt-dir';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if(!file_exists($this->encryptedDirPath)){
            $output->writeln("<fg=red>Encrypted data directory '{$this->encryptedDirPath}' does not exist</>");
            return Command::FAILURE;
        }

        try{
            /** @var \SplFileInfo $fileInfo */
            foreach (FileFinder::getFileList($this->encryptedDirPath) as $filePath) {

                $this->fileEncryptor->decryptFile(
                    $filePath,
                    str_replace($this->encryptedDirPath, $this->decryptedDirPath, $filePath),
                    $input->getArgument('password')
                );

            }
        }
        catch(\SodiumException $e){
            if($e->getMessage() === 'a PHP string is required'){
                $output->writeln('<fg=red>A wrong password provided</>');
                return Command::FAILURE;
            }

            $output->writeln("<fg=red>{$e->getMessage()}</>");
            return Command::FAILURE;
        }

        $output->writeln('<fg=green>Successfully decrypted</>');
        return Command::SUCCESS;
    }
}