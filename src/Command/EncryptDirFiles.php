<?php

namespace Infotechnohelp\Symfony\FileEncryptorBundle\Command;

use Infotechnohelp\Symfony\FileSystemBundle\Service\FileFinder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class EncryptDirFiles extends FileEncryptorCommand
{
    protected static $defaultName = 'file-encryptor:encrypt-dir';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if(!file_exists($this->decryptedDirPath)){
            $output->writeln("<fg=red>Decrypted data directory '{$this->decryptedDirPath}' does not exist</>");
            return Command::FAILURE;
        }

        /** @var \SplFileInfo $fileInfo */
        foreach (FileFinder::getFileList($this->decryptedDirPath) as $filePath) {

            $this->fileEncryptor->encryptFile(
                $filePath,
                str_replace($this->decryptedDirPath, $this->encryptedDirPath, $filePath),
                $input->getArgument('password')
            );

        }

        $output->writeln('<fg=green>Successfully encrypted</>');
        return Command::SUCCESS;
    }
}