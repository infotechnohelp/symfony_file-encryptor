<?php

namespace Infotechnohelp\Symfony\FileEncryptorBundle\Command;

use Infotechnohelp\Symfony\FileEncryptorBundle\Service\FileEncryptor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class FileEncryptorCommand extends Command
{
    protected static $defaultName = 'file-encryptor:base-command';

    protected FileEncryptor $fileEncryptor;

    protected string $encryptedDirPath;

    protected string $decryptedDirPath;

    public function __construct(
        string $name = null, FileEncryptor $fileEncryptor, ParameterBagInterface $params
    )
    {
        parent::__construct($name);

        $this->fileEncryptor = $fileEncryptor;
        $this->encryptedDirPath = $params->get('file-encryptor.encryptedDirPath');
        $this->decryptedDirPath =  $params->get('file-encryptor.decryptedDirPath');
    }

    protected function configure()
    {
        $this
            ->addArgument('password', InputArgument::REQUIRED, 'Encryption password');
    }
}