<?php

namespace Infotechnohelp\Symfony\FileEncryptorBundle\Service;

class FileEncryptor
{
    const CUSTOM_CHUNK_SIZE = 8192;

    private function prepareEncryptionKey(string $password)
    {
        $passwordLength = strlen($password);

        $stringMultiplier = ceil(SODIUM_CRYPTO_SECRETSTREAM_XCHACHA20POLY1305_KEYBYTES // 32
            / $passwordLength);

        return substr(str_repeat($password, $stringMultiplier), 0, 32);
    }

    private function createOutputDirIfRequired(string $outputFilename)
    {
        $outputDirectory = dirname($outputFilename);

        if (!is_dir($outputDirectory)) {
            mkdir($outputDirectory, 0777, true); // true for recursive create
        }
    }

    /**
     * @ref https://stackoverflow.com/q/11716047
     */
    public function encryptFile(
        string $inputFilename, string $outputFilename, string $key
    ): bool
    {
        $iFP = fopen($inputFilename, 'rb');
        $this->createOutputDirIfRequired($outputFilename);
        $oFP = fopen($outputFilename, 'wb');

        [$state, $header] = sodium_crypto_secretstream_xchacha20poly1305_init_push(
            $this->prepareEncryptionKey($key)
        );

        fwrite($oFP, $header, 24); // Write the header first:
        $size = fstat($iFP)['size'];
        for ($pos = 0; $pos < $size; $pos += self::CUSTOM_CHUNK_SIZE) {
            $chunk = fread($iFP, self::CUSTOM_CHUNK_SIZE);
            $encrypted = sodium_crypto_secretstream_xchacha20poly1305_push($state, $chunk);
            fwrite($oFP, $encrypted, self::CUSTOM_CHUNK_SIZE + 17);
            sodium_memzero($chunk);
        }

        fclose($iFP);
        fclose($oFP);
        return true;
    }


    /**
     * @ref https://stackoverflow.com/q/11716047
     */
    function decryptFile(
        string $inputFilename, string $outputFilename, string $key
    ): bool
    {
        $iFP = fopen($inputFilename, 'rb');
        $this->createOutputDirIfRequired($outputFilename);
        $oFP = fopen($outputFilename, 'wb');

        $header = fread($iFP, 24);
        $state = sodium_crypto_secretstream_xchacha20poly1305_init_pull($header, $this->prepareEncryptionKey($key));
        $size = fstat($iFP)['size'];
        $readChunkSize = self::CUSTOM_CHUNK_SIZE + 17;
        for ($pos = 24; $pos < $size; $pos += $readChunkSize) {
            $chunk = fread($iFP, $readChunkSize);
            [$plain, $tag] = sodium_crypto_secretstream_xchacha20poly1305_pull($state, $chunk);
            fwrite($oFP, $plain, self::CUSTOM_CHUNK_SIZE);
            sodium_memzero($plain);
        }
        fclose($iFP);
        fclose($oFP);
        return true;
    }
}